package test;

public class StringEqualsStringMultipleTypes {
    public void main(String[] args){
        Obj s1 = "hej";
        String s2 = "hej";
        if("hej" == "hejsanfdd"){
            System.out.println("TEST");
        }
        if("hej".equals("hej2")){
            System.out.println("TEST");
        }
        if("hej" == s1){
            System.out.println("TEST");
        }
        if("hej" == s2){
            System.out.println("TEST");
        }
    }
}
