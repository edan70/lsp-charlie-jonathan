package server;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/* Test will */
class AnalyzerTest {

    static Analyzer analyzer;
    @BeforeAll
    static void setupTests(){
        analyzer = new Analyzer();
    }

    List<Integer> getSemanticTokens(File file) throws FileNotFoundException{
        //analyze file
        InputStream fileStream = new FileInputStream(file);
        analyzer.analyze(fileStream);

        //get result
        return analyzer.getTestTokens();
    }

    void testSemanticToken(File file, List<Integer> expected) throws FileNotFoundException {
        Assertions.assertEquals(expected, getSemanticTokens(file));
    }

    void testSemanticTokenError(File file, List<Integer> expected) throws FileNotFoundException {
        Assertions.assertNotEquals(expected, getSemanticTokens(file));
    }

    /* TESTS */

    @Test
    void semanticToken1() throws FileNotFoundException {
        File file = new File("testFiles/StringEqualsString.java");
        List<Integer> expected = new ArrayList<Integer>(Arrays.asList(3, 35, 1, 8, 1, 1, 31, 1, 8, 1, 2, 8, 1, 8, 1, 1, 4, 1, 8, 1));
        testSemanticToken(file, expected);
    }

    @Test
    void semanticToken2() throws FileNotFoundException {
        File file = new File("testFiles/StringEqualsStringMultipleTypes.java");
        List<Integer> expected = new ArrayList<Integer>(Arrays.asList(3, 35, 1, 8, 1, 3, 11, 20, 0, 0, 0, 21, 1, 17, 1, 2, 8, 1, 17, 1, 1, 32, 1, 7, 1, 2, 8, 1, 7, 1, 1, 23, 1, 10, 1, 2, 8, 1, 10, 1, 1, 11, 11, 0, 0, 0, 12, 1, 0, 1, 2, 8, 1, 0, 1, 1, 4, 1, 8, 1));
        testSemanticToken(file, expected);
    }

    @Test
    void semanticToken3() throws FileNotFoundException {
        File file = new File("testFiles/BracketsColorSimple.java");
        List<Integer> expected = new ArrayList<Integer>(Arrays.asList(1, 35, 1, 0, 1, 1, 16, 1, 15, 1, 0, 5, 1, 15, 1, 0, 8, 1, 15, 1, 0, 4, 1, 2, 1, 1, 18, 1, 15, 1, 0, 6, 1, 19, 1, 2, 12, 1, 19, 1, 1, 8, 1, 2, 1, 1, 4, 1, 0, 1));
        testSemanticToken(file, expected);
    }

    @Test
    void semanticToken4() throws FileNotFoundException {
        File file = new File("testFiles/BracketsColorHard.java");
        List<Integer> expected = new ArrayList<Integer>(Arrays.asList(0, 63, 1, 2, 1, 0, 9, 1, 15, 1, 0, 5, 1, 15, 1, 0, 8, 1, 15, 1, 0, 4, 1, 6, 1, 0, 7, 1, 15, 1, 0, 6, 1, 19, 1, 0, 29, 1, 19, 1, 0, 1, 1, 6, 1, 0, 1, 1, 2, 1));
        testSemanticToken(file, expected);
    }

    /*ERROR TESTS */

    @Test
    void semanticTokenError1() throws FileNotFoundException {
        File file = new File("testFiles/StringEqualsString.java");
        List<Integer> expected = new ArrayList<Integer>(Arrays.asList(1,1,1,1,1));
        testSemanticTokenError(file, expected);
    }
}