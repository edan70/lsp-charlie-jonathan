package server;

import org.eclipse.lsp4j.*;
import org.eclipse.lsp4j.services.*;
import org.tinylog.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;


public class LangServer implements LanguageServer, LanguageClientAware, WorkspaceService {
    private LanguageClient client;
    private final TextDocumentService textDocumentService;

    public LangServer(){
        this.textDocumentService = new JavaTextDocumentService();
    }

    @Override
    public void connect(LanguageClient client) {
        this.client = client;
        //debugTextService.setClient(client);  //DEBUG
        client.logMessage( new MessageParams(MessageType.Warning, "INIT HELLO WORLD FROM LSP4J!"));
        Logger.info("In LangServer's connect()");
    }


    @Override
    public CompletableFuture<InitializeResult> initialize(InitializeParams params) {
        System.out.println("initialize");
        client.logMessage( new MessageParams(MessageType.Warning, "HELLO WORLD FROM LSP4J!"));
        Logger.info("In LangServer's initialize()");
        ServerCapabilities capabilities = new ServerCapabilities();

        /* Semantic highlighting caps
            https://javadoc.io/doc/org.eclipse.lsp4j/org.eclipse.lsp4j/latest/org/eclipse/lsp4j/SemanticTokensLegend.html
            https://microsoft.github.io/language-server-protocol/specifications/specification-3-16/#version_3_16_0
        */
        List<String> semTokenTypes = getSemTokenTypes();
        List<String> semTokenModifiers = getSemTokenModifiers();
        capabilities.setSemanticTokensProvider(new SemanticTokensWithRegistrationOptions(new SemanticTokensLegend(semTokenTypes, semTokenModifiers), false, true));



        capabilities.setTextDocumentSync(TextDocumentSyncKind.Full);
        capabilities.setCodeActionProvider(false);
        if(ArgumentHandler.isHover()) {
            capabilities.setHoverProvider(true);
        }
        return CompletableFuture.completedFuture(new InitializeResult(capabilities));
    }

    private List<String> getSemTokenTypes(){
        List<String> semTokenTypes = new ArrayList<>(22);
        semTokenTypes.add(SemanticTokenTypes.Namespace);    //0
        semTokenTypes.add(SemanticTokenTypes.Type);         //1
        semTokenTypes.add(SemanticTokenTypes.Class);        //2
        semTokenTypes.add(SemanticTokenTypes.Enum);         //3
        semTokenTypes.add(SemanticTokenTypes.Interface);    //4
        semTokenTypes.add(SemanticTokenTypes.Struct);       //5
        semTokenTypes.add(SemanticTokenTypes.TypeParameter);//6
        semTokenTypes.add(SemanticTokenTypes.Parameter);    //7
        semTokenTypes.add(SemanticTokenTypes.Variable);     //8
        semTokenTypes.add(SemanticTokenTypes.Property);     //9
        semTokenTypes.add(SemanticTokenTypes.EnumMember);   //10
        semTokenTypes.add(SemanticTokenTypes.Event);        //11
        semTokenTypes.add(SemanticTokenTypes.Function);     //12
        semTokenTypes.add(SemanticTokenTypes.Method);       //13
        semTokenTypes.add(SemanticTokenTypes.Macro);        //14
        semTokenTypes.add(SemanticTokenTypes.Keyword);      //15
        semTokenTypes.add(SemanticTokenTypes.Modifier);     //16
        semTokenTypes.add(SemanticTokenTypes.Comment);      //17
        semTokenTypes.add(SemanticTokenTypes.String);       //18
        semTokenTypes.add(SemanticTokenTypes.Number);       //19
        semTokenTypes.add(SemanticTokenTypes.Regexp);       //20
        semTokenTypes.add(SemanticTokenTypes.Operator);     //21
        return semTokenTypes;
    }

    private List<String> getSemTokenModifiers(){
        List<String> semTokenModifiers = new ArrayList<>(10);
        semTokenModifiers.add(SemanticTokenModifiers.Declaration);      //0
        semTokenModifiers.add(SemanticTokenModifiers.Definition);       //1
        semTokenModifiers.add(SemanticTokenModifiers.Readonly);         //2
        semTokenModifiers.add(SemanticTokenModifiers.Static);           //3
        semTokenModifiers.add(SemanticTokenModifiers.Deprecated);       //4
        semTokenModifiers.add(SemanticTokenModifiers.Abstract);         //5
        semTokenModifiers.add(SemanticTokenModifiers.Async);            //6
        semTokenModifiers.add(SemanticTokenModifiers.Documentation);    //7
        semTokenModifiers.add(SemanticTokenModifiers.DefaultLibrary);   //8
        return semTokenModifiers;
    }

    @Override
    public CompletableFuture<Object> shutdown() {
        return null;
    }

    @Override
    public void exit() {
        Logger.info("Exiting...");
    }


    @Override
    public TextDocumentService getTextDocumentService() {
        return this.textDocumentService;
    }

    @Override
    public WorkspaceService getWorkspaceService() {
        return this;
    }

    @Override
    public void cancelProgress(WorkDoneProgressCancelParams params) {
        Logger.info("cancelProgress");
    }

    @Override
    public void didChangeConfiguration(DidChangeConfigurationParams params) {
        Logger.info("didChangeConfigurationParams");
    }

    @Override
    public void didChangeWatchedFiles(DidChangeWatchedFilesParams params) {
        Logger.info("didChangeWatchedFiles");
    }
}
