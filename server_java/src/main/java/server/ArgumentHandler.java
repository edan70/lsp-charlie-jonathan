package server;

public class ArgumentHandler {
    private static boolean STDIO = false;
    private static boolean FOR_COLORS = true;
    private static boolean BRACKET_COLORS = true;
    private static boolean STRING_EQ_COLORS = true;
    private static boolean HOVER = true;
    private static final String helpMessage = "           _                 _  _       _           \n" +
            "  _____  _| |_ ___ _ __   __| |(_)     | |___ _ __  \n" +
            " / _ \\ \\/ / __/ _ \\ '_ \\ / _` || |_____| / __| '_ \\ \n" +
            "|  __/>  <| ||  __/ | | | (_| || |_____| \\__ \\ |_) |\n" +
            " \\___/_/\\_\\\\__\\___|_| |_|\\__,_|/ |     |_|___/ .__/ \n" +
            "                             |__/            |_|    \n\n" +
            "usage: java -jar lsp.jar [options]\n\n" +
            "optional arguments:\n" +
            "    -h, --help\t\t\tshows this help message and exits\n" +
            "    --stdio\t\t\trun on stdio instead of socket\n" +
            "    --no-for-colors\t\tdisable for color iterator highlighting\n" +
            "    --no-bracket-colors\t\tdisable bracket highlighting\n" +
            "    --no-string-eq-colors\tdisable string equality highlighting\n" +
            "    --no-hover\t\t\tdisable hover menu";


    public static boolean analyze(String[] args) throws IllegalArgumentException{
        for(String arg : args){
            switch(arg){
                case "-h":
                case "--help":
                    System.out.println(helpMessage);
                    return true;
                case "--stdio":
                    STDIO = true;
                    break;
                case "--no-for-colors":
                    FOR_COLORS = false;
                    break;
                case "--no-bracket-colors":
                    BRACKET_COLORS = false;
                    break;
                case "--no-string-eq-colors":
                    STRING_EQ_COLORS = false;
                    break;
                case "--no-hover":
                    HOVER = false;
                    break;
                default:
                    throw new IllegalArgumentException("ERROR: '" + arg + "' is not a valid argument");
            }
        }
        return false;
    }

    public static boolean isStdio(){
        return STDIO;
    }

    public static boolean isForColors() {
        return FOR_COLORS;
    }

    public static boolean isBracketColors() {
        return BRACKET_COLORS;
    }

    public static boolean isStringEqColors() {
        return STRING_EQ_COLORS;
    }
    public static boolean isHover() {
        return HOVER;
    }
}
