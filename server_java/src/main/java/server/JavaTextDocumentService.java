package server;

import org.eclipse.lsp4j.*;
import org.eclipse.lsp4j.jsonrpc.CompletableFutures;
import org.eclipse.lsp4j.services.TextDocumentService;
import org.tinylog.Logger;

import java.io.FileNotFoundException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

public class JavaTextDocumentService implements TextDocumentService {
    //private LanguageClient client;
    private final Analyzer analyzer;
    public JavaTextDocumentService(){
        analyzer = new Analyzer();
    }

    @Override
    public CompletableFuture<Hover> hover(HoverParams params) {
        Logger.info("Entered hover ");
        Logger.info(params);
        return CompletableFutures.computeAsync(cancelChecker -> {
                cancelChecker.checkCanceled();
                return new Hover(new MarkupContent("markdown", "# This is a hover example\n" +
                        "This proves that LSP works in multiple editors."));
        });
    }

    @Override
    public void didOpen(DidOpenTextDocumentParams params) {
        Logger.info("Yeah it did open");
        try {
            analyzer.analyze(new URI(params.getTextDocument().getUri()));
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void didChange(DidChangeTextDocumentParams params) {
        Logger.info("Yeah it did change. Should I rebuild?");
        //Alternate method to get source code
        String sourceCode = params.getContentChanges().get(0).getText();

        //analyzer.analyze(new URI(params.getTextDocument().getUri()));
        analyzer.analyze(sourceCode);
    }




    @Override
    public void didClose(DidCloseTextDocumentParams params) {
        Logger.info("didClose");
    }

    @Override
    public void didSave(DidSaveTextDocumentParams params) {
        Logger.info("didSave");
    }

    @Override
    public CompletableFuture<SemanticTokens> semanticTokensRange(SemanticTokensRangeParams params) {
        /* Docs here: https://microsoft.github.io/language-server-protocol/specifications/specification-3-16/#version_3_16_0 */
        return CompletableFutures.computeAsync(cancelToken -> {
            cancelToken.checkCanceled();
            Logger.info("Yeah I'm painting rn");
            List<Integer> data;
            try {
                data = analyzer.getTestTokens();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return null;
            }
            SemanticTokens st = new SemanticTokens(data);
            cancelToken.checkCanceled();
            return st;
        });
    }
}
