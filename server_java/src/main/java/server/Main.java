package server;

import org.eclipse.lsp4j.jsonrpc.Launcher;
import org.eclipse.lsp4j.launch.LSPLauncher;
import org.eclipse.lsp4j.services.LanguageClient;
import org.eclipse.lsp4j.services.LanguageClientAware;
import org.eclipse.lsp4j.services.LanguageServer;
//import org.extendj.JavaCompiler;
import org.tinylog.Logger;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Main {
    public static ArgumentHandler argHandler;

    /**
     * main method, launches an LSP
     *
     * @param args if first argument is "stdio" LSP operates in terminal
     */
    public static void main(String[] args) {
        Logger.info("LSP4J main() is started");
        InputStream input;
        OutputStream output;
        Socket socket;

        try {
            if(ArgumentHandler.analyze(args))
                //User entered '-h'. Program should display help message and quit
                return;

            if (ArgumentHandler.isStdio()) {
                input = System.in;
                output = System.out;
            } else {
                String port = "6010";
                ServerSocket serverSocket = new ServerSocket(Integer.parseInt(port));
                socket = serverSocket.accept();
                input = socket.getInputStream();
                output = socket.getOutputStream();
            }
            LanguageServer server = new LangServer();
            Launcher<LanguageClient> launcher = LSPLauncher.createServerLauncher(server, input, output);
            LanguageClient client = launcher.getRemoteProxy();
            ((LanguageClientAware) server).connect(client);
            launcher.startListening();
        } catch (IOException | IllegalArgumentException e) {
            e.printStackTrace();
        }
    }
}
