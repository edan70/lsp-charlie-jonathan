package server;

import beaver.Parser;
import org.eclipse.lsp4j.CodeLens;
import org.eclipse.lsp4j.Command;
import org.eclipse.lsp4j.Position;
import org.eclipse.lsp4j.Range;
import org.extendj.ast.*;
import org.tinylog.Logger;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;

public class Analyzer {
    private Program program;

    public void analyze(URI uri){
        URL url = null;
        InputStream inputStream = null;
        try {
            url = uri.toURL();
            inputStream = url.openStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
        analyze(inputStream);
    }

    public void analyze(String codeString){
        InputStream inputStream = new ByteArrayInputStream(codeString.getBytes());
        analyze(inputStream);
    }

    /* Main analyze method */
    public void analyze(InputStream inputStream){
        try{
            JavaParser parser = Program.defaultJavaParser();
            Logger.info("Finished building AST!");
            CompilationUnit cu = parser.parse(inputStream, "Code parser");
            program = new Program(new List<>(cu));
        } catch (IOException | Parser.Exception e) {
            e.printStackTrace();
        }
    }


    public ArrayList<Integer> getTestTokens() throws FileNotFoundException {
        ArrayList<Integer> data = new ArrayList<>(5);
        if(program == null)
            throw new FileNotFoundException("program not defined");
        //System.out.println(program.dumpTree());
        //System.out.println(program.semanticTokens().size());
        /* LSP uses relative line number. So next line = lastLine + token.getLine()
        * TODO: Fix semantic tokens that span along multiple lines
        * */
        int lastLine = 0;
        int lastColumn = 0;
        for(Token token : program.semanticTokens()){
            if(!ArgumentHandler.isBracketColors() &&
                    token.getHighlightType() == Token.HighlightType.BRACKET_COLOR){
                continue;
            }
            if(!ArgumentHandler.isForColors() &&
                    token.getHighlightType() == Token.HighlightType.FOR_COLOR){
                continue;
            }
            if(!ArgumentHandler.isStringEqColors() &&
                    token.getHighlightType() == Token.HighlightType.STRING_EQ_COLOR){
                continue;
            }
            int tokenLine = token.getLine();
            int tokenColumn = token.getStart();
            int additionalLines = tokenLine - lastLine;
            if(additionalLines != 0){
                lastColumn = 0;
            }
            int additionalColumns = tokenColumn - lastColumn;
            lastLine = tokenLine;
            lastColumn = tokenColumn;

            data.add(additionalLines); //line
            data.add(additionalColumns); //start
            data.add(token.getLength()); //length
            data.add(token.getType()); //type
            data.add(token.getModifier()); //modifier
            Logger.info(token);
        }
        return data;
    }
}
