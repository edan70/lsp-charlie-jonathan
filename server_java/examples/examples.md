# Examples

## For loop variables
Have you stumbled upon a bug without finding it, and finally realized you have misplaced one of the for-loop variables with the other?

To more easily find these bug, we color each for loop in different colors to visually aid you of the variable differences. Have a look at `Triangle.java` in this folder.

## Finding Brackets Pair
Sometimes it's hard to find the matching bracket (or parenthesis). Our semantic highlighting feature helps you to quickly find the pair of brackets.

There are other help tools for this, but we used this as a very simple way to demonstrate the power of syntax highlighting and semantic highlighting. (very exaggerated)

Think of this code:
```java
public class BracketsSemanticHighlighting {public void main(String[] args){
    int i = 0;
    while(){while(){while(){while(){
    }}}while(){}}
        }
```
### 1. ✅ With our feature
A question like **Add `int test = 0;` after the third while-statement** is easier to do with our coding highlight .
Test it for yourself in `BracketsSemanticHighlighting.java`.

## How to compare String with String
Java code should always compare two strings by using `"".equals("")`

### 1. ✅ Correct way

```java
public class GoodCompare {
    public void main(String[] args){
        if("hej".equals("hej")){
            System.out.println("good");
        }
    }
}
```
### 1. ❌ Bad ways
```java
public class BadCompare {
    public void main(String[] args){
        Obj s1 = "hej";
        String s2 = "hej";
        if("hej" == "hej"){
            System.out.println("Bad");
        }
        if("hej" == s1){
            System.out.println("Bad");
        }
        if("hej" == s2){
            System.out.println("Bad");
        }
    }
}
```
### 1. Test it!
[Test the extension!](../../README.md) Then you can open `AllStringCompareString.java` (same folder as this example) and test.