public class Triangle{
	public static void main(String[] args){
		printTriangle();
	}

	//Prints a triangle like this:
	//       *
    //      ***
	//     *****
	//    *******
	static void printTriangle(){
		int rows = 8;
		int treeWidth= 1;
		int spaceWidth = 7;
		for(int i = 0; i < rows; i++){
			for(int j = 0; j < spaceWidth; j++){
				System.out.print(" ");
			}
			for(int j = 0; j < treeWidth; i++){
				System.out.print("*");
			}
			treeWidth += 2;
			spaceWidth -= 1;
			System.out.println();
		}
	}
}
