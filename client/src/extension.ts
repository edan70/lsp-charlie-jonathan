/* --------------------------------------------------------------------------------------------
 * Copyright (c) Microsoft Corporation. All rights reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 * ------------------------------------------------------------------------------------------ */

import * as path from 'path';
import { workspace, ExtensionContext } from 'vscode';
import { Socket } from 'net';

import {
	LanguageClient,
	LanguageClientOptions,
	ServerOptions,
	StreamInfo,
	TransportKind
} from 'vscode-languageclient/node';
import { ChildProcess } from 'child_process';

let client: LanguageClient;

export function activate(context: ExtensionContext) {
	const socketPort = "6010";
	const javaJarModule = context.asAbsolutePath(
		path.join('server_java', 'lsp.jar')
	);
	const serverOptionsJava: ServerOptions = {
		command: "/usr/bin/java",
        args: ["-jar", javaJarModule, socketPort]
	};

	//(() => Promise<ChildProcess | StreamInfo | MessageTransports | ChildProcessInfo>);
	//Connect to an lready running Language Server with sockets on localhost.
	//TODO: Both start socket with random port, start java and pass that port to LSP. Help?: https://nodejs.org/api/child_process.html#subprocessstdin
	function serverOptionsSocket(): Promise<StreamInfo> {
		const socket = new Socket();
		const client = socket.connect(socketPort);
		return Promise.resolve({
			writer: client,
			reader: client
		});
	}

	const serverOptionsJava2: ServerOptions = {
		command: "/usr/bin/java",
        args: ["-jar", javaJarModule, "--stdio"]
	};

	// Options to control the language client
	const clientOptions: LanguageClientOptions = {
		// Register the server for plain text documents
		documentSelector: [{ scheme: 'file', language: 'java' }],
		synchronize: {
			// Notify the server about file changes to '.clientrc files contained in the workspace
			fileEvents: workspace.createFileSystemWatcher('**/.clientrc')
		}
	};

	// Create the language client and start the client.
	client = new LanguageClient(
		'lsp-charlie-jonathan',
		'LSP Charlie/Jonathan',
		serverOptionsSocket,
		clientOptions
	);

	// Start the client. This will also launch the server
	client.start();
}

export function deactivate(): Thenable<void> | undefined {
	if (!client) {
		return undefined;
	}
	return client.stop();
}
