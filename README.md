# Java LSP with Semantic Highlighting 🔦 & Code Lens 👁
A LSP Server + Extension for VS Code
## Features
+ Semantic Highlighting - Highlights each for loop variable in different color.

+ Semantic Highlighting - Highlights each brackets pair different color.

+ Semantic Highlighting - Highlights when using String == String incorrectly.

+ Hover Tool Tip - Only a Test. Try LSP feature in multiple IDE. (Hover supports more IDE compared to Semantic Highlighting)


## Examples 
To further understand the features, please refer to the `server_java/examples` folder.

## Get started
clone this repo by using `git clone git@bitbucket.org:edan70/lsp-charlie-jonathan.git --recursive`. Remember to include `--recursive`. 

(If you have missed `--recursive` and get error that ExtendJ is not installed, please run `git submodule init && git submodule update`)
### VS Code
1. Open repo `lsp-charlie-jonathan` in VS Code.
2. Install plugins: Run `npm install` in terminal.
3. Build: First time you start the project, run `npm run build-start`. This builds the server and then starts everything. 
4. Launch the extension: In VS Code, go to Debug Tab and click play using the "Launch Client" alternative. Alternatively, press `F5`. If you get error while starting, please press *ignore*.
5. Test: Now you have a new VS Code Window with the extension and LSP server running. Open a .java file to test (examples provided in `server_java/examples`). **Note that it can take some time to load the extension.** (It's loaded when *LSP charlie/jonathan* is shown in the selection panel of the terminal's output tab.)

### 2nd IDE
Follow the instructions in the [extendj-lsp-nvim repo](https://github.com/Chasarr/extendj-lsp-nvim)

CREDITS
-------
We use the following main dependencies (which may have dependencies with other licenses):
- [ExtendJ](https://extendj.org/): provided as Open Source under the Modified BSD License
- [LSP4J](https://github.com/eclipse/lsp4j): Licensed under two licenses: EPL-2.0 OR BSD-3-Clause. 
- [vscode-languageclient](https://github.com/Microsoft/vscode-languageserver-node): licensed under [MIT](https://github.com/Microsoft/vscode-languageserver-node/blob/main/License.txt)

All other smaller dependencies are listed in [package.json](package.json), [client/package.json](client/package.json), [server_java/build.gradle](server_java/build.gradle), [server_java/extendj-lsp/build.gradle](server_java/extendj-lsp/build.gradle)


Files from the the following folders in the repository come from:
- server_java/extendj-lsp: [Analysis Template](https://bitbucket.org/extendj/analysis-template/src), a modified example repo for ExtendJ, with [license](https://bitbucket.org/extendj/analysis-template/src/master/LICENSE)
- lps-charlie-jonathan: from [vscode-extension-samples](https://github.com/Microsoft/vscode-extension-samples), licensed under the MIT License.


LICENCE
-------
[License](./LICENSE) 
